### River-Crossing

This exercise tests your understanding of search algorithms and the technique
of solving problems by searching. It also requires you to (partially) define
a search problem. In particular, you have to think (not very hard) about state
representation.

In this exercise you will solve a popular puzzle. There are many variants of
it, and we'll use the "missionaries and cannibals" version.

You're trying to get 6 people across a river; 3 missionaries and 3 cannibals.
The boat can carry at most two people at a time. The catch is that the
cannibals must never outnumber the missionaries on one side of the river.

[Here's a game where you can try to solve this puzzle](http://www.learn4good.com/kids-games/puzzle/boat.htm).

The search.zip file (river package) contains four classes:

- Problem
- Algorithm
- State
- Test

Inside the zip there is also a lib/search-util.jar containing two important
interfaces (in the river package):

- RcState
- RcAction

Note that lib/search-util.jar contains the source for most of the classes.

**Your task is to implement an algorithm that finds the _shortest_ solution
for getting everyone safely across the river.**

You will have to implement the two (state and action) interfaces and complete
the two (problem and algorithm) classes. There are many possible algorithms
one could use to solve this problem. Choose wisely.

Note that the actions are pretty trivial in this scenario. That's why the
interface is **empty**. So you can choose any representation you like, as long
as it "implements" the interface.

You can run the `Test` class to test your algorithm. It will make sure you
produce the correct solution, and it will look at the number of goal tests and
the number of node expansions to assess your choice of algorithm.

As usual, you can import the code in eclipse (or some other IDE) and run
the test from there, or you can use ant to run the build script
(with `ant test`).

Create any helper classes you deem appropriate.


### Touring Romania

In this exercise you will implement the A\* algorithm and solve a problem that
has become a classic in AI - how do you find the quickest route to Bucharest?

The search.zip file (romania package) contains:

- TrProblem
- Astar
- Test

Inside the zip there is also a search-util.jar containing two classes (in the
romania package):

- City
- DriveAction

`City` is an enum of all the cities (`City.ARAD`). DriveAction is a class
that holds two cities: `action.from()` and `action.to()`. You create an
instance with `new DriveAction(startCity, goalCity)`.

The code for most of the classes in lib/search-util.jar is included in the
jar if you feel the need to examine it.

The problem has been fully set up for you, and __your task is to implement the
A\* algorithm by filling in the `Astar.solve(Problem)` method__.

You can run the `Test` class to test your algorithm. It will make sure you
produce the correct solution. It will also look at which nodes were
expanded during the search.

Tip: You **should definitely** "run" the algorithm manually at least once
before trying to code it! You should definitely **not** try to implement
the algorithm by blindly translating the pseudo code from the book.

As usual, you can import the code in eclipse (or some other IDE) and run
the test from there, or you can use ant to run the build script
(with `ant test`).

Create any helper classes you deem appropriate.

`TrProblem` also contains a few different heuristics:

- `TrProblem.STRAIGHT_LINE_DISTANCE_HEURISTIC`
- `TrProblem.MANHATTAN_DISTANCE_HEURISTIC`
- `TrProblem.MIN_NODES_TRAVERSED_HEURISTIC`

The first is the one that is used in the book when explaining the algorithm.
You are encouraged to experiment with the others as well, and see
how they perform. You should try to predict what to expect before running them.
If you want to examine a heuristic directly, you can call
`heuristic.estimatedCostAt(city)`.


#### Straight-Line Distance

This is the example heuristic from the book. It uses the distance needed if
you could fly (or drive in a straight line).


#### Manhattan

This heuristic adds the distances in longitude and latitude. For example,
for Arad the value is 720+250.


#### Min Nodes Traversed

This heuristic counts the minimum number of nodes (cities) that must be
traversed. For example, for Arad the value is 3.
