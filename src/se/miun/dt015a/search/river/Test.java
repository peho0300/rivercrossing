package se.miun.dt015a.search.river;

import se.miun.dt015a.search.SearchAlgorithm;
import se.miun.dt015a.search.SearchProblem;

/**
 * Tests your problem definition and algorithm for solving the problem.
 * The test requires you to find the shortest solution. It will also check
 * that your algorithm is reasonably efficient.
 *
 * @author Christoffer Fink
 */
public class Test extends RiverTest {
  @Override
  public SearchProblem<RcState, RcAction> getProblemInstance() {
    return new Problem();
  }
  @Override
  public SearchAlgorithm<RcState, RcAction> getAlgorithmInstance() {
    return new Algorithm();
  }
}
