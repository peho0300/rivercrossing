package se.miun.dt015a.search.river;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import se.miun.dt015a.search.SearchProblem;
import se.miun.dt015a.search.SearchAlgorithm;
import se.miun.dt015a.search.Successor;
import se.miun.dt015a.search.Solution;

/**
 * An implementation of an algorithm that solves the River-Crossing.
 * Implement an appropriate algorithm by filling out the solve method.
 *
 * @author Christoffer Fink
 */
public class Algorithm implements SearchAlgorithm<RcState, RcAction> {

  // You must choose an appropriate algorithm.
  // Feel free to add any helper classes you might need during the search.
  // What's important is that you can produce a Solution.
  // The solution is a list of Successor objects, starting with the Successor
  // holding the initial state.
	
private List<State> previousStates;
	
  @Override
  public Solution<RcState, RcAction> solve(SearchProblem<RcState, RcAction> problem) {
		
	//Solution<RcState, RcAction> solution=BreadthFirstSearch(problem);
	Solution<RcState, RcAction> solution=IterativeDeepeningSearch(problem);
	
	return solution;

    // You'll want to use the following methods. These are only examples:
    // RcState state = problem.initialState();
    // boolean goal = problem.goal(state);
    // Set<Successor<State, RcAction> succs = problem.successors(state);
  }
  
  private Solution<RcState,RcAction> IterativeDeepeningSearch(SearchProblem<RcState, RcAction> problem) {
		int i=0;
		List<Successor<RcState, RcAction>> s = null;
		for(i=5;i<100;i++) {
			previousStates = new ArrayList<State>();
			System.out.println("Deeper limit: " + i);
			Successor<RcState,RcAction> start = Successor.zeroth(problem.initialState());
			s = RecursiveDLS(start,problem,i,0);
			if(s!=null)
				break;
				
		}
		
		Collections.reverse(s);
		
		for(Successor<RcState,RcAction> su:s) {
			State temp = (State)su.state();
			System.out.println(temp.toString());
		}
		
		return new Solution<RcState,RcAction>(s);
  }
  
  private List<Successor<RcState,RcAction>> RecursiveDLS(Successor<RcState,RcAction> succs,SearchProblem<RcState, RcAction> problem,int limit,int depth) {
	  
	  List<Successor<RcState,RcAction>> s = null;
	  
	  if(!previousStates.contains((State)succs.state())) {
		  
		  System.out.println(depth + ": Not duplicate state - " + ((State)succs.state()).toString());
	  
		  previousStates.add((State)succs.state());
		  
		  if(!problem.goal(succs.state())) {
			  if(depth<limit) {
				  for(Successor<RcState,RcAction> su:problem.successors(succs.state())) {
					  s = RecursiveDLS(su,problem,limit,depth+1);
					  if(s!=null) {
						  s.add(succs);
						  break;
					  }
				  }
			  }
		  }
		  else {
			  System.out.println(depth + ": Goal");
			  s = new ArrayList<Successor<RcState,RcAction>>();
			  s.add(succs);
		  }
	  }
	  else
		  System.out.println(depth + ": Duplicate state - "  + ((State)succs.state()).toString());
	  return s;
  }



/*
function DEPTH-LIMITED-SEARCH(problem, limit ) returns a solution, or failure/cutoff
return RECURSIVE-DLS(MAKE-NODE(problem.INITIAL-STATE), problem, limit )
function RECURSIVE-DLS(node, problem, limit ) returns a solution, or failure/cutoff
if problem.GOAL-TEST(node.STATE) then return SOLUTION(node)
else if limit = 0 then return cutoff
else
cutoff occurred?←false
for each action in problem.ACTIONS(node.STATE) do
child ←CHILD-NODE(problem, node, action)
result ←RECURSIVE-DLS(child , problem, limit − 1)
if result = cutoff then cutoff occurred?←true
else if result = failure then return result
if cutoff occurred? then return cutoff else return failure
*/
  class Node
  {
	Successor<RcState,RcAction> succs;
	Node parent;
	
	
	public Node(Node parent,Successor<RcState, RcAction> succs) {
		this.succs = succs;
		this.parent = parent;
	}
	
	public Node getParent() {
		return parent;
	}
	public void setParent(Node parent) {
		this.parent = parent;
	}
	public Successor<RcState, RcAction> getSuccs() {
		return succs;
	}
	public void setSuccs(Successor<RcState, RcAction> succs) {
		this.succs = succs;
	}
	
   };

private Solution<RcState,RcAction> BreadthFirstSearch(SearchProblem<RcState, RcAction> problem) {
	
	Successor<RcState,RcAction> start = Successor.zeroth(problem.initialState());
	Queue<Node> frontier = new LinkedList<Node>();
	List<State> explored = new ArrayList<State>();
	List<Successor<RcState,RcAction>> solution_list = new ArrayList<Successor<RcState,RcAction>>();
	frontier.add(new Node(null,start));
	Node succ = null;
	
	while(true) {
		if(frontier.size()==0) {
			System.out.println("End of frontier");
			break;
		}
		
		System.out.println("Poll");
		succ = frontier.poll();
		
		explored.add((State)succ.getSuccs().state());
		
		if(problem.goal(succ.getSuccs().state())) {
			System.out.println("Goal State - " + ((State)succ.getSuccs().state()).toString() );
			break;
		}		
		
		for(Successor<RcState,RcAction> s:problem.successors(succ.getSuccs().state())) {
			if(!explored.contains((State)s.state())&&!containsState(frontier,(State)s.state())) {
				System.out.println("New State - " + ((State)s.state()).toString() );
				frontier.add(new Node(succ,s));
			}
			else
				System.out.println("Explored State - " + ((State)s.state()).toString() );
		}
	}
	
	Node cNode = succ;
	while(cNode.getParent()!=null) {
		solution_list.add(cNode.getSuccs());
		cNode=cNode.getParent();
	}
	solution_list.add(cNode.getSuccs());
	
	Collections.reverse(solution_list);
	
	return new Solution<RcState,RcAction>(solution_list);
}

private boolean containsState(Queue<Node> q,State s) {
	boolean tf=false;
	for(Node n:q) {
		if(((State)n.succs.state()).equals(s)) {
			tf=true;
			break;
		}
	}
	return tf;
}

}
/*
function BREADTH-FIRST-SEARCH(problem) returns a solution, or failure
node ←a node with STATE = problem.INITIAL-STATE, PATH-COST = 0
if problem.GOAL-TEST(node.STATE) then return SOLUTION(node)
frontier ←a FIFO queue with node as the only element
explored ←an empty set
loop do
if EMPTY?( frontier) then return failure
node←POP( frontier ) /* chooses the shallowest node in frontier 
add node.STATE to explored
for each action in problem.ACTIONS(node.STATE) do
child ←CHILD-NODE(problem, node, action)
if child .STATE is not in explored or frontier then
if problem.GOAL-TEST(child .STATE)
*/