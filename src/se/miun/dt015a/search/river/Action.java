package se.miun.dt015a.search.river;

public class Action implements RcAction {

	private int nrCannibals;
	private int nrMissionaries;
	private boolean moveLeft;
	
	public Action(int nrCannibals, int nrMissionaries, boolean moveLeft) {
		super();
		this.nrCannibals = nrCannibals;
		this.nrMissionaries = nrMissionaries;
		this.moveLeft = moveLeft;
	}

	public int getNrCannibals() {
		return nrCannibals;
	}

	public int getNrMissionaries() {
		return nrMissionaries;
	}

	public boolean isMoveLeft() {
		return moveLeft;
	}
	
    public String toString() {
    	if(moveLeft)
    		return "Moving " + nrCannibals + " Cannibals and " + nrMissionaries + " Missionaries Left";
    	else
    		return "Moving " + nrCannibals + " Cannibals and " + nrMissionaries + " Missionaries Right";
    }
	
}
