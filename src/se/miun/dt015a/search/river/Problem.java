package se.miun.dt015a.search.river;

import java.util.HashSet;
import java.util.Set;

import se.miun.dt015a.search.util.AbstractSearchProblem;

/**
 * The definition of the River-Crossing problem.
 * Your algorithm will use this definition when solving the problem.
 *
 * @author Christoffer Fink
 */
public class Problem extends AbstractSearchProblem<RcState, RcAction> {

  public Problem() {
    super(new State(3,0,3,0,true));
  }

  // Checks whether the given state satisfies the goal test.
  // Have we reached the goal?
  @Override
  public boolean goal(RcState state) {
	  if(state.cannibalsRight()==3&&state.missionariesRight()==3)
		  return true;
	  else	 
		  return false;
  }
  
  private boolean failure(RcState state, RcAction action) {
	  State newState = (State) resultingState(state,action);
	  
	  if(newState.cannibalsLeft()>newState.missionariesLeft()||newState.cannibalsRight()>newState.missionariesRight())
		  if(newState.missionariesLeft()==3||newState.missionariesRight()==3)
			  return false;
		  else
			  return true;
	  else
		  return false;
  }

  // Taking an action in some state results in a new state.
  // Given the current state and an action, return the new state.
  @Override
  public RcState resultingState(RcState state, RcAction action) {
	  
	int mLeft = state.missionariesLeft();
	int mRight = state.missionariesRight();
	int cLeft = state.cannibalsLeft();
	int cRight = state.cannibalsRight();
	
	Action a = (Action) action;
	
	if(a.isMoveLeft()) {
		mLeft+=a.getNrMissionaries();
		cLeft+=a.getNrCannibals();
		mRight-=a.getNrMissionaries();
		cRight-=a.getNrCannibals();
	}
	else {
		mLeft-=a.getNrMissionaries();
		cLeft-=a.getNrCannibals();
		mRight+=a.getNrMissionaries();
		cRight+=a.getNrCannibals();	
	}
	
    return new State(mLeft,mRight,cLeft,cRight,a.isMoveLeft());
    
  }
  
  

  // The set of actions that can be taken in the given state.
  @Override
  public Set<RcAction> possibleActions(RcState state) {
	Set<RcAction> actions = new HashSet<RcAction>();
	
	int thisSideCannibals = state.cannibalsLeft();
	int thisSideMissionaries = state.missionariesLeft();
	
	if(state.boatRight()) {
		thisSideCannibals = state.cannibalsRight();
		thisSideMissionaries = state.missionariesRight();
	}
	
	//Move 1 of each
	if(thisSideCannibals>0&&thisSideMissionaries>0){
		Action a = 	new Action(1,1,state.boatRight());
		if(!failure(state,a))
			actions.add(a);
	}
		
	//Move 1 Cannibal
	if(thisSideCannibals>0){
		Action a = 	new Action(1,0,state.boatRight());
		if(!failure(state,a))
			actions.add(a);
	}
	
	//Move 1 Missionary
	if(thisSideMissionaries>0){
		Action a = 	new Action(0,1,state.boatRight());
		if(!failure(state,a))
			actions.add(a);
	}
		
	//Move 2 Cannibals
	if(thisSideCannibals>1){
		Action a = 	new Action(2,0,state.boatRight());
		if(!failure(state,a))
			actions.add(a);
	}		
	
	//Move 2 Missionaries
	if(thisSideMissionaries>1){
		Action a = 	new Action(0,2,state.boatRight());
		if(!failure(state,a))
			actions.add(a);
	}	

	
    return actions;
  }
}
