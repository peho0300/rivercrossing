package se.miun.dt015a.search.river;

/**
 * An implementation of the River-Crossing state.
 * Don't forget to also create an implementation of RcAction.
 *
 * @author Christoffer Fink
 */
public class State implements RcState {
  // 1. Add any fields you deem appropriate for representing the state.
  // 2. Modify the methods below so that they return accurate information
  //    about the state.
  // 3. You may add constructors (and additional methods) as necessary.
	
	int mLeft;
	int mRight;
	int cLeft;
	int cRight;
	boolean boatLeft;
	
  public State(int mLeft, int mRight, int cLeft, int cRight, boolean boatLeft) {
		super();
		this.mLeft = mLeft;
		this.mRight = mRight;
		this.cLeft = cLeft;
		this.cRight = cRight;
		this.boatLeft = boatLeft;
	}
@Override
  public int missionariesLeft() {
    return mLeft;
  }
  @Override
  public int missionariesRight() {
    return mRight;
  }
  @Override
  public int cannibalsLeft() {
    return cLeft;
  }
  @Override
  public int cannibalsRight() {
    return cRight;
  }
  @Override
  public boolean boatLeft() {
    return boatLeft;
  }
  @Override
  public boolean boatRight() {
    return !boatLeft;
  }
  @Override
  public boolean equals(Object obj) {
	  State otherState = (State)obj;
	  if(	this.mLeft==otherState.missionariesLeft()&&
			this.mRight==otherState.missionariesRight()&&
			this.cLeft==otherState.cannibalsLeft()&&
			this.cRight==otherState.cannibalsRight()&&
			this.boatLeft==otherState.boatLeft())
		  return true;
	  else
		  return false;
  }
  
  public String toString() {
	  
	String s = "Right - Cannibals: " + cLeft + ", Missionaries: " + mLeft + " - Left - Cannibals: " + cRight + " Missionaries: " + mRight + " - Boat is to the ";
	if(boatLeft)
		s+="left";
	else
		s+="right";
	return s;
  }
  
}
