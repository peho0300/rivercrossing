package se.miun.dt015a.search.romania;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import se.miun.dt015a.search.util.Heuristic;
import se.miun.dt015a.search.Successor;
import se.miun.dt015a.search.Solution;
import se.miun.dt015a.search.SearchProblem;
import se.miun.dt015a.search.InformedSearchAlgorithm;

/**
 * A silly example search algorithm.
 * Note that it even ignores the heuristic.
 *
 * @author Christoffer Fink
 */
public class RandomSearch<S,A> implements InformedSearchAlgorithm<S,A> {

  private final Random rng = new Random();

  @Override
  public Solution<S,A> solve(SearchProblem<S,A> problem, Heuristic<S> unused) {

    S state = problem.initialState();
    List<Successor<S,A>> path = new ArrayList<>();
    path.add(new Successor<>(state, null, 0));

    while (!problem.goal(state)) {
      Successor<S,A> successor = pickRandomSuccessor(problem, state);
      path.add(successor);
      state = successor.state();
    }
    return new Solution<S,A>(path);
  }

  private Successor<S,A> pickRandomSuccessor(SearchProblem<S,A> p, S s) {
    List<Successor<S,A>> successors = new ArrayList<>(p.successors(s));
    return successors.get(rng.nextInt(successors.size()));
  }
}
