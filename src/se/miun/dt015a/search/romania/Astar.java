package se.miun.dt015a.search.romania;

import se.miun.dt015a.search.SearchProblem;
import se.miun.dt015a.search.InformedSearchAlgorithm;
import se.miun.dt015a.search.Successor;
import se.miun.dt015a.search.Solution;
import se.miun.dt015a.search.util.Heuristic;

/**
 * An implementation of the A* algorithm.
 * Complete the solve method.
 *
 * @author Christoffer Fink
 */
public class Astar implements InformedSearchAlgorithm<City, DriveAction> {

  @Override
  public Solution<City, DriveAction> solve(SearchProblem<City, DriveAction> problem, Heuristic<City> heuristic) {
    // Silly example.
    return new RandomSearch<City, DriveAction>()
      .solve(problem, heuristic); // Your code here.
  }

  // For experimentation.
  public static void main(String[] args) {
    City start = City.LUGOJ;
    City goal = City.ARAD;
    TrProblem problem = TrProblem.getInstance(start, goal);

    City origin = problem.initialState();
    System.out.println("Starting in the city of " + origin);
    System.out.println("From there I can get to:");
    for (Successor<City,DriveAction> successor : problem.successors(origin)) {
      System.out.println("  " + successor.state());
    }

    Astar astar = new Astar();
    System.out.println("Starting search...");
    Solution<City,DriveAction> solution = astar.solve(problem, null);
    System.out.println("Found a solution with total cost " + solution.cost());
    System.out.println("Solution visits these cities: " + solution.states());
  }
}
