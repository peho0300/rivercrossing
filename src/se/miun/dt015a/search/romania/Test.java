package se.miun.dt015a.search.romania;

import se.miun.dt015a.search.InformedSearchAlgorithm;

/**
 * Tests your implementation of A*.
 *
 * @author Christoffer Fink
 */
public class Test extends RomaniaTest {

  public InformedSearchAlgorithm<City, DriveAction> getAlgorithm() {
    return new Astar();
  }

@Override
public InformedSearchAlgorithm<City, DriveAction> getAlgorithmInstance() {
	// TODO Auto-generated method stub
	return null;
}
}
